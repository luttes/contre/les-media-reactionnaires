.. index::
   ! Asma Mhalla

.. _asma_mhalla:

=================================================================
**Asma Mhalla**
=================================================================

- https://www.institutmontaigne.org/experts/asma-mhalla
- https://www.polytechnique.edu/en/directory/mhalla-asma


Biographie
==============

Spécialiste des enjeux politiques et géopolitiques de la Tech, Asma Mhalla
est membre du LAP (Laboratoire d'Anthropologie Politique) de l'EHESS/CNRS
et enseignante à Columbia GC, Sciences Po et l’École Polytechnique.

Ses travaux portent notamment sur les nouvelles formes de pouvoir et de puissance
entre Etats et Géants Technologiques (BigTech) dans le champ civil et militaire
(guerres hybrides), les enjeux démocratiques et de gouvernance des réseaux sociaux,
les dimensions géopolitique et idéologique de l'IA ainsi que la souveraineté
technologique.

Docteure en études politiques, Asma Mhalla est chercheure au Laboratoire
d’Anthropologie Politique de l'Ehess.

Politologue spécialiste des enjeux politiques et géopolitiques de la Tech et
de l’IA, elle conseille gouvernements et institutions dans leur politique
publique technologique.

Elle a produit et animé, à l’été 2023, l’émission « CyberPouvoirs » sur
France Inter qui a été très remarquée.


Livres 2024
===========

- :ref:`asma_technopolitique`

Articles 2024
==================

- https://www.liberation.fr/idees-et-debats/asma-mhalla-les-geants-de-la-tech-deploient-une-vision-du-monde-coherente-comme-dans-toute-folie-20240302_HSKCCIKOEFFKTNSYZB2FBGVSEE/
- https://usbeketrica.com/fr/article/asma-mallah-les-big-tech-ont-un-projet-de-controle-total-du-monde-et-du-futur


Articles 2023
==================

- https://www.institutmontaigne.org/expressions/les-reseaux-sociaux-nourrissent-ils-les-populismes
- https://www.polytechnique-insights.com/en/columns/geopolitics/splinternet-when-geopolitics-fractures-cyberspace/
- https://www.lesechos.fr/idees-debats/editos-analyses/elon-musk-batisseur-du-futur-aux-confins-du-nouvel-ordre-mondial-2040777
