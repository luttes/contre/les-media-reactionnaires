.. index::
   pair: Asma Mhalla; Technopolitique, Comment la technologie fait de nous des soldats (2024)
   ! Technopolitique

.. _asma_technopolitique:

=====================================================================================
**Technopolitique, Comment la technologie fait de nous des soldats** par Asma Mhalla
=====================================================================================

- https://www.seuil.com/ouvrage/technopolitique-asma-mhalla/9782021548549
- https://usbeketrica.com/fr/article/asma-mallah-les-big-tech-ont-un-projet-de-controle-total-du-monde-et-du-futur
- https://www.france.tv/france-5/la-grande-librairie/saison-16/5713689-technopolitique-l-essai-fracassant-d-asma-mhalla.html

.. figure:: images/technopolitique.webp


Intelligence artificielle, réseaux sociaux, implants cérébraux, satellites,
métavers…

Le choc technologique sera l’un des enjeux clés du xxie siècle et les géants
américains, les « BigTech », sont à l’avant-garde.

Entités hybrides, ils remodèlent la morphologie des États, redéfinissent les
jeux de pouvoir et de puissance entre nations, interviennent dans la guerre,
tracent les nouvelles frontières de la souveraineté.

S’ils sont au cœur de la fabrique de la puissance étatsunienne face à la Chine,
ils sont également des agents perturbateurs de la démocratie.

De ces liens ambivalents entre BigTech et « BigState » est né un nouveau
Léviathan à deux têtes, animé par un désir de puissance hors limites.

Mais qui gouverne ces nouveaux acteurs privés de la prolifération technologique ?

A cette vertigineuse question, nous n’avons d’autre choix que d’opposer
l’innovation politique !

S’attaquant à tous les faux débats qui nous font manquer l’essentiel,
Asma Mhalla ose ainsi une thèse forte et perturbante : les technologies de
l’hypervitesse, à la fois civiles et militaires, font de chacun d’entre nous,
qu’on le veuille ou non, des soldats.

Nos cerveaux sont devenus l’ultime champ de bataille.

Il est urgent de le penser car ce n’est rien de moins que le nouvel ordre mondial
qui est en jeu, mais aussi la démocratie.

Docteure en études politiques, Asma Mhalla est chercheure au Laboratoire
d’Anthropologie Politique de l'Ehess.

Politologue spécialiste des enjeux politiques et géopolitiques de la Tech et
de l’IA, elle conseille gouvernements et institutions dans leur politique
publique technologique.

Elle a produit et animé, à l’été 2023, l’émission « CyberPouvoirs » sur
France Inter qui a été très remarquée.

