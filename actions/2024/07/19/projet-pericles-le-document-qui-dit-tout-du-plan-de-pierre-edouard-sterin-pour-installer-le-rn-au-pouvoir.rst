.. index::
   ! Périclès

.. _pericles_2024_07_19:

======================================================================================================================================================================================================================
2024-07-19 Projet Périclès (Patriotes/Enracinés/Résistants/Identitaires/Chrétiens/Libéraux/Européens/Souverainistes): le document qui dit tout du plan de Pierre-Édouard Stérin pour installer le RN au pouvoir 
======================================================================================================================================================================================================================

- https://www.humanite.fr/politique/bien-commun/projet-pericles-le-document-qui-dit-tout-du-plan-de-pierre-edouard-sterin-pour-installer-le-rn-au-pouvoir


Rédigé à la manière d’un business plan de start-up, le document élaboré 
par le milliardaire Pierre-Édouard Stérin et ses proches au sein de 
Périclès décrit, étape par étape, l’installation à tous les échelons du 
pouvoir d’une alliance entre l’extrême droite et la droite libérale-conservatrice. 

L’Humanité en dévoile de larges extraits qui, dans les circonstances 
politiques actuelles, sont plus que jamais d’intérêt public.


1. Rappel du plan
=========================

Projet Périclès, acronyme : Patriotes/Enracinés/Résistants/Identitaires/Chrétiens/Libéraux/Européens/Souverainistes

Fondateurs et administrateurs : Pierre-Édouard Stérin (Otium Capital et 
Fonds du bien commun), François Durvye (Otium Capital), Alban du Rostu (Fonds du bien commun)

Notre projet découle d’un ensemble de valeurs clés. 
Valeurs que nous promouvons : liberté individuelle et d’entreprendre/Propriété privée/Subsidiarité/Le vrai, 
le bien, le beau/La famille, base de la société/Place particulière du 
christianisme/Enracinement dans un terroir/Fierté de notre histoire, 
notre identité, notre culture/Unité, cohésion et confiance. 

Tendances que nous combattons : hyperétatisme/Socialisme 
et assistanat/Lois liberticides/Wokisme/Refus des limites/Laïcité 
agressive/Refus de la préférence nationale/Islamisme/Immigration incontrôlée

