.. index::
   pair: FAFAM ; 2024-01-05

.. _gafam_2024_01_05:

=================================================================
2024-01-05 "ouin ouin, les grands méchants GAFAM"
=================================================================

- https://borispaing.fr/blog/2024-01-05-les-grands-mechants-gafam.html


Liens
======

- https://scu
- https://delta.chat/fr/ttlebutt.nz/

Texte
======

"ouin ouin, les grands méchants GAFAM"

J'ai toujours du mal à écouter des gens se plaindre qu'ils sont censurés
sur Youtube ou ailleurs.

C'est quoi ces gens qui n'ont pas le courage de déserter ?

Tous ces youtubeurs pro-expression, là, ça leur dirait pas, un jour, de
faire un truc…

Genre de quitter ces plateformes tous en même temps ?

Alors, c'est sûr, quelqu'un comme Greg Tabibian, il n'a pas la même capacité
à bouger les foules que quelqu'une comme Kim Kardashian. M'enfin ça n'interdit
pas d'essayer.

Ces gens pourraient faire bien moins que ça… le "minimum syndical", déjà.

Car ils se plaignent qu'ils sont censurés sur des plateformes verrouillées,
mais on ne les voit jamais faire quoi que ce soit pour reprendre leurs destins
en main, genre :

- créer une mailing list,
- ouvrir un Mastodon,
- parler de ScuttleButt
- parler du Fediverse,
- parler de Delta.chat,
- faire une publicité régulière pour ces moyens de communication
  (bandeau, description, appel à l'action en fin de vidéo, etc…)

voire :

- financer des plateformes vidéos libres

Car certains vidéastes lèvent quand même des sommes non négligeables…

Ça ne leur viendrait pas à l'idée de se côtiser pour reverser chacun une infime
partie de leurs revenus à des plateformes concurrentes genre CrowdBunker,
ou PeerTube, ou MediaGoblin, ou autre… ?

Je crois qu'ils ne se rendent pas compte à quel point une plateforme comme
Facebook, à la base, ce n'est quand même pas grand chose, techniquement
— c'est juste la bête fusion d'un lecteur de flux RSS, d'un système de
microblogging, d'un annuaire à la Pages Blanches ou Copains d'avant, et d'un
tchat, et qu'en se côtisant ils pourraient faire apparaître des concurrents
sérieux. Mathieu nous l'a bien prouvé quand il a créé CrowdBunker en un
rien de temps, en partant de zéro, sans ressource, et sur son seul temps libre.

Le truc le plus basique et le moins couillu que pourraient faire tous ces
pignoux de la liberté d'expression, c'est installer une bête instance PeerTube
(Thinkerview l'a bien fait), et publier leurs vidéos dessus 1 j (ou même
ne serait-ce qu'1 h) avant de les publier sur Youtube. Et faire de même
avec Mastodon/ScuttleButt/mailing list vs Twitter/Facebook/Threads.

C'est quand même hallucinant, d'entendre gémir, notamment, des gens qui
veulent que la République Française se libère du joug des États-Unis
d'Amérique, mais qui ne font pas le minimum pour se sortir eux-mêmes du joug
des entreprises américaines.

Alors même qu'ils habitent au pays de VLC, Framasoft, PeerTube et
CrowdBunker ! Un pays où tu peux trouver des développeurs de haut niveau
qui acceptent de bosser pour presque rien.

Quand je les écoute, j'ai l'impression d'entendre quelqu'un qui se plaindrait
en permanence des pesticides, tout en évitant soigneusement d'acheter un seul
aliment bio, même quand il est au même prix que son homolégume pesticidé.

C'est un peu comme si, moi, je me plaignais que sur TF1 et BFMTV on n'a rien
d'autre que de la propagande, tout en regardant ces chaînes.

Faut être cohérent 5 minutes.

Éviter d'attraper ce que FramaBlog, en 2006, avait appelé le "syndrome Bayrou".
tiré du PoliTIC'Show

Ne dit-on pas que "charité bien ordonnée commence par soi-même" ?

En même temps, que peut-on attendre de chroniqueurs politiques, dont le fond
de commerce est la pleurniche ?

Alors, des fois, ils tentent un truc. Genre ils publient sur Odysee, et font
un peu la promotion de cette plateforme. Mais ils ne le font pas vraiment
gratuitement : c'est en partie parce qu'il y a déjà des gens dessus,
et qu'ils pensent qu'ils augmenteront leur portée ainsi. Ce n'est pas
très courageux. Et ce n'est rien d'autre que se choisir un autre maître,
en espérant qu'il sera meilleur que l'ancien. Ils sont bien déçus, ces
créateurs de contenu, quand ils se rendent compte que la censure est aussi
possible (et effective) sur Odyssee.

Les gens adorent la citation de Coluche "Quand on pense qu'il suffirait que
les gens n'achètent plus pour que ça ne se vende pas !", mais rechignent
à dépenser 480 kilocalories de jus de cerveau pour installer (ou faire
installer) un PeerTube et arrêter de valoriser les GAFAM.

Un certain François — dont je n'ai rien à carrer — fait preuve de plus
de lucidité qu'eux quand il suggère aux gameurs de s'intéresser au mode de
production des jeux vidéos. C'est relativement superflu dans ce cas — osef
des jeux vidéos — mais ce serait très pertinent dans le cadre du rapport
des youtubeurs aux plateformes vidéos.

En ce moment, ils voient Elon Musk comme le sauveur de leur liberté
d'expression. Bon, on verra combien de temps durera l'illusion. Mon pari est
qu'il ne faudra probablement plus attendre que quelques mois avant qu'Elon Musk
et X se fassent rattrapés par la cybernétique du Capital. Rebelle ou pas,
Musk sera mis au pas. Et quand ils verront leur Messie crucifié, peut-être
les opprimés du moment redescendront-ils sur Terre.

M'eeeeenfin… il y a 17 ans déjà, on leur proposait de se libérer de leurs
chaînes micro$oftiennes et pas un seul n'a bougé le petit doigt, sous prétexte
qu'il leur fallait Window$ pour exécuter WINWORD.EXE et World of Warcraft ;
ils pleurent maintenant parce qu'ils se font remplacer par ChatGPT.

Cause, conséquence
============================

Les gens comprennent pourquoi il ne faut pas nourrir les trolls, mais ils ne
comprennent pas pourquoi il ne faut pas nourrir les ogres.

On peut sans prendre trop de risque parier qu'en 2040 on retrouvera les mêmes,
ou leurs remplaçants — genre les actuels TikTokers —, à nous larmoyer
un truc du style::

    BOU-HOU-HOU… la EvilCorp elle est mé-chante avec Mooouuuaaa…

J'espère que ce jour là il me restera suffisamment de liberté d'expression
pour pouvoir leur dire : "on t'avait prévenu, Ducon ; tu ne nous a pas
écouté, alors bien fait pour ta gueule".

Ceux qui acceptent de remettre leur liberté d'expression entre les mains
d'autres gens ne méritent pas de s'exprimer du tout.

Note 
======

cet article est une réaction aux propos de Nicolas Vidal à la table ronde
Liberté d'expression organisée par l'UPR.
Arrête tes chialeries, Nico ; ta tentative d'auto-victimisation ne peut
pas marcher ici et maintenant, car tu n'as aucune prétention légitime à l'impuissance.
