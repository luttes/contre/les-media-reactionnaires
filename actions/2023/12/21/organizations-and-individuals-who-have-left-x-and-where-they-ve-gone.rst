.. index::
   ! Organizations and individuals who have left X (and where they've gone)

.. _moeller_2023_12_21:

===============================================================================================================
2023-12-21 **Organizations and individuals who have left X (and where they've gone)** by Erik Moeller
===============================================================================================================

- https://social.coop/@eloquence/111616991527339531

Hi #twitter / #x folks checking your trusty Mastodon account while the 
deadbird site is down because Elon chewed on the wrong wires:

If you leave by December 31, you can still say that you left the nazi 
bar behind in 2023. ;-) Join all the folks who've done so already.

- https://docs.google.com/spreadsheets/d/1p5crRGK0Y9MuFsfkyOBEC--3wXxFTd71fPCUcIFPXK0/edit#gid=0
