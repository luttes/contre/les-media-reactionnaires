
.. _ach_2023_12_19:

===================================================================================================
2023-12-19 **ACH leaves Twitter, commits to contributing to better DH social media elsewhere**
===================================================================================================

- https://ach.org/blog/2023/12/19/ach-leaves-twitter-commits-to-contributing-to-better-dh-social-media-elsewhere/
   
Introduction
=============
   
The ACH is leaving Twitter: we’re locking our Twitter account, and will
no longer post, read, nor reply there. 

ACH members and the broader digital humanities community can find us on 
Bluesky (bsky.app/profile/ach.bsky.social), Mastodon (hcommons.social/@ach), 
our website (ach.org); and for ACH members, our newsletter (members.ach.org/join).

Why we’re leaving Twitter
============================

We leave Twitter because of its rapidly increasing harms—both policies that
are explicitly **racist, sexist, transphobic, and harmful in many additional
ways**; and policy (non)implementation and culture that make the space harmful
or impossible for many to use. 

This is in keeping with the ACH’s mission, which states::

    “ACH recognizes that this work is inherently and inextricably
    sociopolitical, and thus advocates for social change through the use of
    computers and related technologies in the study of humanistic subjects.”

We recognize this is not a decision everyone can make—for example, unemployed
and precarious community members can’t afford to miss anywhere jobs might be
posted. 

If we don’t put opportunities and other info on Twitter, people don’t
have to expose themselves to the most blatant harms to get that info. 

We leave Twitter to contribute toward ending that lock-in. 

Please let the organizations you’re part of know, if their moving off 
Twitter would help you be able to leave too. 

If you’re an organization or individual who can safely do so, we encourage 
you to help everyone leave Twitter by leaving yourself.
